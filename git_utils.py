from operator import sub
import subprocess
import re

class GitAdd:   
    files = []

    def __init__(self):
        pass

    @classmethod
    def parse_and_store_results(cls, text):
        res = re.findall('(modified\:[\s]+([\S]+))',text)
        for subtext in res:
            cls.files.append(subtext[1])

        
    @classmethod
    def main(cls):
        res = subprocess.run("git status",shell=True, capture_output=True)        
        raw = res.stdout.decode("utf-8")
        cls.parse_and_store_results(raw)
        cls.prompt_user_to_select_files()
        

    @staticmethod
    def clean_user_input(text):
        if "," in text:
            items = text.split(",")
            items = [int(item.strip("")) for item in items]
            print(f"current: {items}")
            return items
        else:
            pass
        return int(text)
    
    @classmethod
    def execute_user_input(cls, selected_files):
        for selection in selected_files:
            # print(cls.files[selection])
            res = subprocess.run(f"git add {cls.files[selection]}",shell=True, capture_output=True)        


    @classmethod
    def prompt_user_to_select_files(cls):
        # print(f"{str(i) + cls.files[i] for i in range(0, len(cls.files)) }")
        
        for i in range(0, len(cls.files)):
            print(f"{i}: {cls.files[i]}")
        user_input = input(f"Please select files to git add: ")
        user_input = user_input.strip("")
        items = cls.clean_user_input(user_input)
        cls.execute_user_input(items)



if __name__ == "__main__":
    main = GitAdd()
    main.main()

